<?php
    class Carts extends Database
    {
        function getCarts()
        {
            $statement = $this->pdo->prepare("SELECT c.*, p.name FROM cart c INNER JOIN products p ON c.product_id = p.id");
            $statement->execute();

            return $statement->fetchAll(PDO::FETCH_ASSOC);
        }

        function getCartById($id)
        {
            $statement = $this->pdo->prepare("SELECT c.*, p.* FROM cart c INNER JOIN products p ON c.product_id = p.id WHERE c.id = :id");
            $params = array(":id" => $id);

            $statement->execute($params);

            return $statement->fetch(PDO::FETCH_ASSOC);
        }        

        function getCartByProductId($id)
        {
            $statement = $this->pdo->prepare("SELECT c.*, p.* FROM cart c INNER JOIN products p ON c.product_id = p.id WHERE c.product_id = :id");
            $params = array(":id" => $id);

            $statement->execute($params);

            return $statement->fetchAll(PDO::FETCH_ASSOC);
        }

        function store($productId, $userId, $quantity, $total_price)
        {
            $statement = $this->pdo->prepare("INSERT INTO cart(product_id, user_id, quantity, total_price) VALUES(:product_id, :user_id, :quantity, :total_price)");
            $params = array(":product_id" => $productId, ":user_id" => $userId, ":quantity" => $quantity, "total_price" => $total_price);
            $result = $statement->execute($params);

            return $result;
        }

        function update($product_id, $quantity, $total_price)
        {
            $stmt = $this->pdo->prepare("UPDATE cart SET quantity = :quantity, total_price = :total_price WHERE product_id = :product_id");
            $params = array(":product_id" => $product_id, ":quantity" => $quantity, ":total_price" => $total_price);
            $stmt->execute($params);
        }

        function delete($cartId)
        {
            $stmt = $this->pdo->prepare("DELETE FROM cart WHERE id = :id");
            $params = array(":id" => $cartId);
            $result = $stmt->execute($params);

            return $result;
        }

        function deleteAll()
        {
            $stmt = $this->pdo->prepare("DELETE FROM cart");
            $result = $stmt->execute();

            return $result;
        }

        function purchase($previous_balance, $total_purchase, $shipping_option_fee)
        {
            $stmt = $this->pdo->prepare("INSERT INTO purchases(previous_balance, total_purchase, shipping_option_fee) VALUES(:previous_balance, :total_purchase, :shipping_option_fee)");
            $params = array(":previous_balance" => $previous_balance, ":total_purchase" => $total_purchase, ":shipping_option_fee" => $shipping_option_fee);
            $result = $stmt->execute($params);

            return $result;
        }
    }
?>