<?php
    define('DB_HOST', 'localhost');
    define('DB_NAME', 'test_shopping_cart');
    define('DB_USER', 'root');
    define('DB_PASSWORD', 'admin');

    class Database
    {
        protected $pdo;

        function __construct()
        {
            $this->pdo = new PDO("mysql:host=" . DB_HOST .";dbname=" . DB_NAME, DB_USER, DB_PASSWORD);
        }
    }
?>