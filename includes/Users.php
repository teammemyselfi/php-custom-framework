<?php
    class Users extends Database
    {
        function getUsers()
        {
            $statement = $this->pdo->prepare("SELECT * FROM user");
            $statement->execute();

            return $statement->fetchAll(PDO::FETCH_ASSOC);
        }

        function getUserById($id)
        {
            $statement = $this->pdo->prepare("SELECT * FROM user WHERE id = :id");
            $params = array(":id" => $id);

            $statement->execute($params);

            return $statement->fetch(PDO::FETCH_ASSOC);
        }

        function updateBalance($newBalance)
        {
            $stmt = $this->pdo->prepare("UPDATE user SET balance = :balance");
            $params = array(":balance" => $newBalance);
            $result = $stmt->execute($params);

            return $result;
        }
    }
?>