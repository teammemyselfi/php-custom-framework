<?php
    class Products extends Database
    {
        function getProducts()
        {
            $statement = $this->pdo->prepare("SELECT p.*, AVG(pr.rating) AS rating FROM products p INNER JOIN product_ratings pr ON p.id = pr.product_id GROUP BY id");
            $statement->execute();

            return $statement->fetchAll(PDO::FETCH_ASSOC);
        }

        function getProductById($id)
        {
            $statement = $this->pdo->prepare("SELECT p.*, AVG(pr.rating) AS rating FROM products p INNER JOIN product_ratings pr ON p.id = pr.product_id WHERE p.id = :id GROUP BY id");
            $params = array(":id" => $id);

            $statement->execute($params);

            return $statement->fetch(PDO::FETCH_ASSOC);
        }
    }
?>