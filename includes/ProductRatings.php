<?php
    class ProductRatings extends Database
    {
        function getProductRatings()
        {
            $statement = $this->pdo->prepare("SELECT * FROM products_ratings");
            $statement->execute();

            return $statement->fetchAll(PDO::FETCH_ASSOC);
        }

        function getProductRatingByProductId($product_id)
        {
            $statement = $this->pdo->prepare("SELECT * FROM product_ratings WHERE product_id = :product_id");
            $params = array(":product_id" => $product_id);

            $statement->execute($params);

            return $statement->fetch(PDO::FETCH_ASSOC);
        }

        function store($productId, $rating)
        {
            //$statement = $this->pdo->prepare("UPDATE product_ratings SET rating = :rating WHERE product_id = :product_id");
            $statement = $this->pdo->prepare("INSERT INTO product_ratings(product_id, rating) VALUES(:product_id, :rating)");
            $params = array(":product_id" => $productId, ":rating" => $rating);

            $result = $statement->execute($params);

            return $result;
        }
    }
?>