<?php
    define("ROOT", dirname(dirname(__FILE__)));
    define("DS", "/");
    
    // load classes use in the system
    function autoload($class_name) {
        require_once('includes/' . $class_name . '.php');
    }

    // dynamically load classes when called inside the system
    spl_autoload_register("autoload");
?>