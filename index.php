<?php 
    session_start();
    
    $_SESSION["set_product_rating"] = array();

    require_once("autoload.php");

    (new Users)->updateBalance(100);
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js" type="text/javascript"></script>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
        
        <link rel="stylesheet" href="css/style.css" />

        <title>Shopping Cart</title>
    </head>
    <body>
        <div class="message-container container d-none">
            <div class="card">
                <div class="card-header text-white bg-info">
                    Message
                </div>
                <div class="card-body">
                    
                </div>
            </div>
        </div>
        <br />
        <div class="container">
            <div class="card">
                <div class="card-header text-white bg-info">
                    <div class="row">
                        <div class="col-8">My Balance: $ <span id="my_balance" class="font-weight-bold"></span></div>
                        <div class="col-3">
                            <select id="shipping_option" class="form-control float-right">
                                <option value="">Shipping Option</option>
                                <option value="0">Pick Up (USD 0)</option>
                                <option value="5">UPS (USD 5)</option>
                            </select>
                        </div>
                        <div class="col-1">
                            <button type="button" id="pay_now" class="btn btn-warning float-right">Pay Now</button>
                        </div>
                    </div>                    
                </div>
                <div class="card-body">
                    <table class="table-shopping-cart table">
                        <tr>
                            <th>Name</th>
                            <th>Quantity</th>
                            <th>Total Price</th>
                            <th>Action</th>
                        </tr>
                    </table>
                </div>
                <div class="card-footer text-muted">
                    <span class="float-right">Total: $ <span id="total_price">0</span></span>                  
                </div>
            </div>
        </div>
        <br />
        <div class="container">
            <div class="card">
                <div class="card-header text-white bg-info">
                    Product Catalog
                </div>
                
                <!-- product lists -->
                <div id="product_catalog" class="card-deck mr-2 ml-2 mb-3 mt-3 text-center">
                    
                </div>
            </div>
        </div>

        <!-- MODAL -->
        <div id="modal_info" class="modal" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header bg-primary text-white">
                        <h5 class="modal-title">Information</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p id="modal_text"></p>
                    </div>
                    <div class="modal-footer bg-light">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>

        <script type="text/javascript" src="javascript/scripts.js"></script>
        <script type="text/javascript" src="javascript/star.js"></script>
        <script type="text/javascript" src="javascript/InputSpinner.js"></script>
    </body>
</html>