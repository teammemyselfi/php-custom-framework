<?php
    session_start();
    require_once("../autoload.php");

    if ($_GET["f"] == "getProductRatings")
    {
        echo (new Utilities)->JSONResponse(true, "", (new ProductRatings)->getProductRatings());
    }
    else if ($_GET["f"] == "updateRating")
    {
        $productId = (int)$_POST["product_id"];
        $rating = $_POST["rating"];
        $set_product_rating = $_SESSION["set_product_rating"];
        
        if ($set_product_rating === NULL)
        {
            // do nothing
        }
        // dont add if visitor not unique
        else if (in_array($productId, $set_product_rating) === true)
        {
            echo (new Utilities)->JSONResponse(false, "You have already rated this product.", (new Products)->getProductById($productId));
            return;
        }
    
        $result = (new ProductRatings)->store($productId, $rating);
        
        // add the product rated to session
        array_push($set_product_rating, $productId);
        $_SESSION["set_product_rating"] = $set_product_rating;

        echo (new Utilities)->JSONResponse(true, "", (new Products)->getProductById($productId));
    }
?>