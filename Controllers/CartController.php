<?php
    require_once("../autoload.php");

    if ($_GET["f"] == "getCart")
    {
        echo (new Utilities)->JSONResponse(true, "", (new Carts)->getCarts());
    }
    else if ($_GET["f"] == "addToCart")
    {
        $productId = $_POST["product_id"];
        
        $cart = (new Carts)->getCartByProductId($productId);
        $product = (new Products)->getProductById($productId);
        
        // if product already exist in the cart, update the product quantity and total price
        if (count($cart) > 0)
        {
            $quantity = $cart[0]["quantity"] + 1;
            $total_price = $product["price"] * $quantity;

            $cart = (new Carts)->update($product["id"], $quantity, $total_price);
        }
        // if product not exist, insert a new record
        else
        {
            $user = (new Users)->getUsers();
            
            $cart = (new Carts)->store($product["id"], $user[0]["id"], 1, $product["price"]);
        }

        echo (new Utilities)->JSONResponse(true, "", (new Carts)->getCarts());
    }
    else if ($_GET["f"] == "deleteItemToCart")
    {
        $cartId = $_POST["cart_id"];

        $cart = new Carts();
        $result = $cart->delete($cartId);

        echo (new Utilities)->JSONResponse(true, "", $cart->getCarts());
    }
    else if ($_GET["f"] == "purchase")
    {
        $user = (new Users)->getUsers();

        $total_purchase = (float) $_POST["total_purchase"];
        $shipping_option_fee = (int) $_POST["shipping_option_fee"];
        $previous_balance = ((float) $user[0]["balance"]);
        $new_balance = $previous_balance - $total_purchase - $shipping_option_fee;

        // check if balance is not enough to purchase the item
        if ($new_balance < 0)
        {
            echo (new Utilities)->JSONResponse(false, "Warning", array("Current balance is not enough for this purchase."));
            return;
        }
        else
        {
            // add new purchase
            $cart = new Carts();

            $cartItems = $cart->getCarts();

            $cart->purchase($previous_balance, $total_purchase, $shipping_option_fee);

            // delete all from cart
            $cart->deleteAll();

            // update user balance
            (new Users)->updateBalance($new_balance);
        }

        echo (new Utilities)->JSONResponse(true, "", array("previous_bal" => $previous_balance, "total_purchase" => $total_purchase, "new_bal" => $new_balance, "shipping_option_fee" => $shipping_option_fee, "cart_items" => $cartItems));
    }
    else if ($_GET["f"] == "updateCartItemQuantity")
    {
        $cartId = $_POST["cart_id"];
        $productId = $_POST["product_id"];
        $quantity = intval($_POST["quantity"]);

        $product = (new Products)->getProductById($productId);

        // calculate new price for new quantity
        $total_price = floatval($product["price"]) * $quantity;

        $cart = new Carts();
        $cart->update($productId, $quantity, $total_price);

        echo (new Utilities)->JSONResponse(true, "", (new Carts)->getCarts());
    }
?>